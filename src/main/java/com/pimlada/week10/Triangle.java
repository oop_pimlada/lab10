package com.pimlada.week10;

public class Triangle extends Shape{
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) {
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }
    public double getB() {
        return b;
    }
    public double getC() {
        return c;
    }

    @Override
    public double calArea() {
        double s = (a + b + c) / 2;
        return Math.sqrt(s * (s - this.a) * (s - this.b) * (s - this.c));
    }

    @Override
    public double calPerimeter() {
        return this.a + this.b + this.c;
    }

    @Override
    public String toString() {
        return this.getName() + " a:" + this.a + " b:" + this.b + " c:" + this.c;
    }
}
